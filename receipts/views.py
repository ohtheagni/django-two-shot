from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from .forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.db.models import Count


# Create your views here.


@login_required
def home(request):
    if request.user.is_authenticated:
        receipts = Receipt.objects.filter(purchaser=request.user)
    else:
        receipts = Receipt.objects.none()
    context = {"receipts": receipts}
    return render(request, "receipts/home.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    return render(request, "receipts/create.html", {"form": form})


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(
        receipts__purchaser=request.user
    ).annotate(num_receipts=Count("receipts"))
    return render(
        request, "receipts/category_list.html", {"categories": categories}
    )


@login_required
def account_list(request):
    accounts = Account.objects.filter(
        receipts__purchaser=request.user
    ).annotate(num_receipts=Count("receipts"))
    return render(
        request, "receipts/account_list.html", {"accounts": accounts}
    )


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    return render(request, "receipts/create_category.html", {"form": form})


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    return render(request, "receipts/create_account.html", {"form": form})
