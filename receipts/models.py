from django.db import models
from django.contrib.auth.models import User

# value we apply to receipts such as gas or entertainment
class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="categories"
    )

    def __str__(self):
        return self.name


# The way we paid for it such as credit card or bank account
class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="accounts"
    )

    def __str__(self):
        return self.name


# the way we keep track for accountnig purposes
class Receipt(models.Model):
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(
        max_digits=10, decimal_places=3
    )  # DecimalField used for money
    tax = models.DecimalField(max_digits=10, decimal_places=3)
    date = models.DateTimeField()
    purchaser = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="receipts"
    )
    category = models.ForeignKey(
        ExpenseCategory, on_delete=models.CASCADE, related_name="receipts"
    )
    account = models.ForeignKey(
        Account, on_delete=models.SET_NULL, related_name="receipts", null=True
    )
